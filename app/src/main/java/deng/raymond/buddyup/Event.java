package deng.raymond.buddyup;

/**
 * Event Item
 */
class Event {
    public int id;
    public String activity;
    public String location;
    public String time;
    public String date;
    public int current;
    public int max;
}
