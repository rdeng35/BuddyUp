package deng.raymond.buddyup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Activity File for join_event.xml
 */
public class JoinEvent extends BaseActivity{
    private PopupWindow rowWindow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        setContentView(R.layout.join_event);
        super.onCreate(savedInstanceState);
        if(super.isNetworkAvailable(this)) {
            try {
                JSONObject idData = new JSONObject();
                idData.put("u_id", LoginActivity.userID);

                FillEvent table = new FillEvent(this);
                String result = table.execute(idData).get();

                JSONArray json_result = new JSONArray(result);
                List<Event> event_data = new ArrayList<>();
                for (int i = 0; i < json_result.length(); i++) {
                    JSONObject tempObject = json_result.getJSONObject(i);
                    Event tempEvent = new Event();
                    tempEvent.id = tempObject.getInt("e_id");
                    tempEvent.activity = tempObject.getString("activity_name");
                    tempEvent.location = tempObject.getString("location");
                    tempEvent.time = tempObject.getString("e_time");
                    tempEvent.date = tempObject.getString("e_date");
                    tempEvent.current = tempObject.getInt("people_joined");
                    tempEvent.max = tempObject.getInt("max_people");
                    event_data.add(tempEvent);

                    addTableRow(tempEvent);

                }

            } catch (InterruptedException e) {
                Log.v("BuddyUp", "RECEIVED AN INTERRUPTED EXCEPTION");
            } catch (ExecutionException e) {
                Log.v("BuddyUp", "RECEIVED AN EXECUTION EXCEPTION");
            } catch (ParseException e) {
                Log.v("BuddyUp", "RECEIVED A PARSE EXCEPTION");
            } catch (JSONException e) {
                Log.v("BuddyUp", "RECEIVED A JSON EXCEPTION");
            }
        }
        else
            Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();
    }

    //Add New Row to the Table Dynamically
    @SuppressLint("SimpleDateFormat")
    private void addTableRow(Event item) throws NullPointerException, ParseException
    {
        final TableLayout event_table = (TableLayout) findViewById(R.id.table_layout);
        @SuppressLint("InflateParams")
        final TableRow view = (TableRow)getLayoutInflater().inflate(R.layout.join_event_item, null);

        TextView activity = (TextView)view.findViewById(R.id.table_activity_field);
        activity.setText(item.activity);

        TextView location = (TextView)view.findViewById(R.id.table_location_field);
        location.setText(item.location);

        TextView time = (TextView)view.findViewById(R.id.table_time_field);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        Date inputTime = timeFormat.parse(item.time);
        timeFormat = new SimpleDateFormat("hh:mm aa");
        item.time = timeFormat.format(inputTime);
        time.setText(item.time);

        TextView date = (TextView)view.findViewById(R.id.table_date_field);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date inputDate = dateFormat.parse(item.date);
        dateFormat = new SimpleDateFormat("mm/dd/yy");
        item.date = dateFormat.format(inputDate);
        date.setText(item.date);

        String curMax = item.current + "/" + item.max;
        TextView size = (TextView)view.findViewById(R.id.table_size_count);
        size.setText(curMax);

        Button joinBtn = (Button)view.findViewById(R.id.table_join_btn);
        joinBtn.setId(item.id);

        event_table.addView(view);
    }
    //POST Request to URL and add row to the Table
    class FillEvent extends AsyncTask<JSONObject, Void, String>
    {
        private final Context mContext;
        private String response;

        FillEvent(Context context){
            this.mContext = context;
        }
        protected String doInBackground(JSONObject... params) {
            try {

                JSONObject inputData = params[0];
                URL url = new URL("https://buddyupteam.herokuapp.com/bu_fill_evt");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(inputData.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                StringBuffer sb = new StringBuffer();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String read;

                while ((read = br.readLine()) != null) {
                    sb.append(read);

                }
                br.close();
                response = sb.toString();
            } catch (IOException e) {
                Log.d("BuddyUp", "THREAD ERROR: IO EXCEPTION " + e.toString());

            }
            return response;
        }
    }
    //This Thread Will Leave The Event with UID and Event ID
    class JoinAnEvent extends AsyncTask<JSONObject, Void, String>
    {
        private final Context mContext;
        private String result;

        JoinAnEvent(Context context){
            this.mContext = context;
        }

        protected String doInBackground(JSONObject... params) {

            try{
                JSONObject newData = params[0];

                URL url = new URL ("https://buddyupteam.herokuapp.com/bu_add_user_to_event");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(newData.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String read;

                while((read= reader.readLine())!=null)
                {
                    buffer.append(read);

                }
                reader.close();
                result = buffer.toString();

            }
            catch(IOException e)
            {
                Log.d("BuddyUp", "Error: " + e.toString());
            }

            return result;
        }
    }
    //onClickListener for users who join Events
    public void userJoinEvent (View v) throws JSONException,InterruptedException,ExecutionException, NullPointerException
    {
        //Package JSON to send
        JSONObject inputData = new JSONObject();
        inputData.put("evt_id",v.getId());
        inputData.put("u_id", LoginActivity.userID);

        if(super.isNetworkAvailable(this))
        {
            JoinAnEvent joinEventAction = new JoinAnEvent(this);
            String join_response = joinEventAction.execute(inputData).get();

            //Successfully left, send back to home screen
            if ((Integer.parseInt(join_response)) == 1) {
                Intent intent = new Intent(JoinEvent.this, HomePage.class);
                Toast.makeText(this, "You have been successfully added to this event", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
            //Tell the user, there was an error processing this request
            else
                Toast.makeText(this, "There was an error adding you to the event", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();

    }

    public void displayPopUp (View v) throws NullPointerException
    {
        ScrollView parentLayout = (ScrollView)findViewById(R.id.join_event_layout);
        parentLayout.setAlpha(0.25f);
        LayoutInflater inflater = (LayoutInflater) JoinEvent.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.pop_up_window,
                (ViewGroup) findViewById(R.id.pop_up_layout));
        rowWindow = new PopupWindow(layout, 1000, 1000, true);
        rowWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        TableRow t = (TableRow) v;

        TextView temp;
        String tempString;
        //Build The Popup Window
        TextView popActivityName = (TextView) layout.findViewById(R.id.popActivityText);
        temp = (TextView) t.getChildAt(0);
        tempString = temp.getText().toString();
        popActivityName.setText(tempString);

        TextView popLocation = (TextView) layout.findViewById(R.id.popLocationText);
        temp = (TextView) t.getChildAt(1);
        tempString = temp.getText().toString();
        popLocation.setText(tempString);

        TextView popTime = (TextView) layout.findViewById(R.id.popTimeText);
        temp = (TextView) t.getChildAt(2);
        tempString = temp.getText().toString();
        popTime.setText(tempString);

        TextView popDate = (TextView) layout.findViewById(R.id.popDateText);
        temp = (TextView) t.getChildAt(3);
        tempString = temp.getText().toString();
        popDate.setText(tempString);

        TextView popSize = (TextView) layout.findViewById(R.id.popCountText);
        temp = (TextView) t.getChildAt(4);
        tempString = temp.getText().toString();
        popSize.setText(tempString);

    }

    public void closePopUp (View v) {
        ScrollView parentLayout = (ScrollView)findViewById(R.id.join_event_layout);
        parentLayout.setAlpha(1.0f);
        rowWindow.dismiss();
    }

}


