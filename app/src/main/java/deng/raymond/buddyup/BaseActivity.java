package deng.raymond.buddyup;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Generic Activity that is inherited by all Activities to use custom typeface
 * IMPORTANT: When creating new activities that inherit this one
 *
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityInfo activityInfo;
        try {
            //This will change the Typeface of the header_text from the asset included
            TextView header = (TextView) findViewById(R.id.header_text);
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Xoxoxa.ttf");
            header.setTypeface(tf);


            //Sets the dynamic title for all layouts with header included in XML
            activityInfo = getPackageManager().getActivityInfo(
                    getComponentName(), PackageManager.GET_META_DATA);
            String title = activityInfo.loadLabel(getPackageManager())
                    .toString();
            header.setText(title);
        }
        catch(NullPointerException e )
        {
            Log.v("BuddyUp", "Null Pointer Found from BaseActivity");
        }
        catch(PackageManager.NameNotFoundException e){
            Log.v("BuddyUp","Name Not Found EXCEPTION TOSSED");
        }
    }
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else {
            return false;
        }
    }
}
