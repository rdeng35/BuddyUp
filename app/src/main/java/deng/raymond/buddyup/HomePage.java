package deng.raymond.buddyup;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity file for Main Page
 */
public class HomePage extends BaseActivity implements View.OnClickListener{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.home);
        super.onCreate(savedInstanceState);

        try {
            //This will change the Typeface of the header_text from the asset included
            TextView welcomeSign = (TextView) findViewById(R.id.welcomeLogin);
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/athletic.ttf");
            welcomeSign.setTypeface(tf);
        }
        catch(NullPointerException e){
            Toast.makeText(this, "Typeface Error", Toast.LENGTH_SHORT).show();
        }


    Button create_evt = (Button)findViewById(R.id.create_evt_button);
    if(create_evt!=null) {
        create_evt.setOnClickListener(this);
    }
    Button joinEvt = (Button)findViewById(R.id.join_evt_btn);
    if(joinEvt!=null) {
        joinEvt.setOnClickListener(this);
    }
    Button invite_friend = (Button)findViewById(R.id.my_evt_btn);
    if(invite_friend!=null) {
        invite_friend.setOnClickListener(this);
    }
    Button logout = (Button)findViewById(R.id.logout_btn);
    if(logout!=null) {
        logout.setOnClickListener(this);
    }
}

    @Override
    public void onClick(View v) {
        Intent nextPage = new Intent();

        switch(v.getId()){

            case R.id.create_evt_button:
                nextPage = new Intent(HomePage.this, CreateEvent.class);
                break;
            case R.id.join_evt_btn:
                nextPage = new Intent(HomePage.this, JoinEvent.class);
                break;
            case R.id.my_evt_btn:
                nextPage = new Intent(HomePage.this, MyEvent.class);
                break;
            case R.id.logout_btn:
                LoginActivity.globalemail="";
                LoginActivity.userID=0;
                Toast.makeText(this,"You have successfully logged out", Toast.LENGTH_SHORT).show();
                nextPage = new Intent(HomePage.this, LoginActivity.class);
                break;
        }
        startActivity(nextPage);

    }
}
