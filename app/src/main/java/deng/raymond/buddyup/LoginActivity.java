package deng.raymond.buddyup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends BaseActivity
{

    private Intent nextPage;
    private JSONObject input;
    public static String globalemail;
    public static int userID;
    private int loginTries; //Variable to control how many tries to login

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.login);
        super.onCreate(savedInstanceState);
        loginTries = 0;
        try {
            //This will change the Typeface of the header_text from the asset included
            TextView header = (TextView) findViewById(R.id.bu_login_label);
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/athletic.ttf");
            header.setTypeface(tf);
        }
        catch(NullPointerException e){
            Log.v("BuddyUp", "TYPEFACE COULD NOT BE CHANGED");
        }

        input = new JSONObject();

    }

    class VerifyLogin extends AsyncTask<Void, Void, String>
    {
        private final Context mContext;
        private String response;

        VerifyLogin(Context context){
            this.mContext = context;
        }
        protected String doInBackground(Void... params) {
            try{

                URL url = new URL ("https://buddyupteam.herokuapp.com/bu_login");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(input.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                StringBuffer sb = new StringBuffer();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String read;

                while((read= br.readLine())!=null)
                {
                    sb.append(read);

                }
                br.close();
                response = sb.toString();
            }
            catch(IOException e) {
                Log.d("BuddyUp:", "IO EXCEPTION " + e.toString());

            }

            return response;
        }
    }

    boolean checkInput(String input)
    {
        if(input.matches("[0-9a-zA-Z\\._]+@{1}[0-9a-zA-Z]+\\.{1}(com|edu|net)"))
            return true;
        else
            return false;
    }

    public void loginButton (View v) throws JSONException,InterruptedException,ExecutionException, NullPointerException {
        loginTries++;
        //Function to timeout if user enters 5x incorrect password
        if (loginTries >= 5) {
            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
            alertDialog.setTitle("Too many failed attempts");
            alertDialog.setMessage("Please wait 10 seconds");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            v.setClickable(false);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Button loginBtn = (Button)findViewById(R.id.login_button);
                    loginBtn.setClickable(true);
                }
            }, 10000);
        } else {
            EditText e_text = (EditText) findViewById(R.id.email_input);
            EditText pw_text = (EditText) findViewById(R.id.pw_input);
            String email = e_text.getText().toString();
            String pw = pw_text.getText().toString();

            input.put("email", email);
            input.put("pw", pw);

            if (checkInput(email)) {
                if (super.isNetworkAvailable(this)) {
                    VerifyLogin checkLogin = new VerifyLogin(this);
                    String result = checkLogin.execute().get();
                    int temp = Integer.parseInt(result);
                    if (temp > 0) {
                        globalemail = email;
                        userID = temp;
                        nextPage = new Intent(LoginActivity.this, HomePage.class);
                        Toast.makeText(this, "You've successfully login", Toast.LENGTH_SHORT).show();
                        startActivity(nextPage);
                    } else if (temp == -1) {
                        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        alertDialog.setMessage("Email does not exist \n\nPlease register below");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        alertDialog.setMessage("Incorrect Password");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                } else
                    Toast.makeText(this, "You are not connected to the internet", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "Improper format. Try again!", Toast.LENGTH_SHORT).show();
        }
    }

    public void loginRegister (View v)
    {
        nextPage = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(nextPage);
    }

    public void onBackPressed() {
        Toast.makeText(this, "Back Button does not work on this page \n         Please Register or Login",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
