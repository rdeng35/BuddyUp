package deng.raymond.buddyup;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * THIS PAGE IS USED TO CREATE EVENT
 */
public class CreateEvent extends BaseActivity{

    private Spinner activitySpinner;
    private String activityName;
    private TextView add_activity;
    private ArrayAdapter<String> activityAdapter;
    private List<String> activityList;
    private Calendar myCalendar;
    private EditText edit_Date;
    private EditText edit_Time;
    private EditText location;
    private EditText capacity;
    private final int owner = LoginActivity.userID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        setContentView(R.layout.create_event);
        super.onCreate(savedInstanceState);

        add_activity = (EditText) findViewById(R.id.add_activity);
        setActivitySpinnerItems();

    }

    //OnClick Listener for Register Button
    public void registerEventBtn (View v)throws InterruptedException, ExecutionException,JSONException
    {
        location = (EditText)findViewById(R.id.edit_location);
        capacity = (EditText)findViewById(R.id.edit_people);
        boolean activitySet = true;
        int tempCap = 2;

            if (add_activity.isEnabled()) {
                if (add_activity == null || add_activity.getText().toString().equals("")) {
                    activitySet = false;
                } else
                    activityName = add_activity.getText().toString();
            }

        String result;

        JSONObject sendData = new JSONObject();

        if(capacity.getText().toString().equals("")) {
            Toast.makeText(this,"Capacity cannot be empty, 0, 1",Toast.LENGTH_SHORT).show();
        }
        else
            tempCap = Integer.parseInt(capacity.getText().toString());

        if(activitySet == false || location == null || location.getText().toString().equals("") || edit_Date == null || edit_Time == null)
            Toast.makeText(this, "Empty Entry/Entries Detected. Please try again.", Toast.LENGTH_SHORT).show();
        else if(!checkInput(activityName, edit_Time.getText().toString(),edit_Date.getText().toString(), location.getText().toString()))
            Toast.makeText(this, "Invalid inputs. Try Again", Toast.LENGTH_SHORT).show();
        else if(tempCap > 999)
            Toast.makeText(this, "Capacity limit: 999", Toast.LENGTH_SHORT).show();
        else if(activityName.length() > 30)
            Toast.makeText(this, "Activity limit: 30 characters", Toast.LENGTH_SHORT).show();
        else if(location.length() > 30)
            Toast.makeText(this, "Location limit: 30 characters", Toast.LENGTH_SHORT).show();
        else {
            sendData.put("activName", activityName);
            sendData.put("owner", Integer.toString(owner));
            sendData.put("location", location.getText().toString());
            sendData.put("date", edit_Date.getText().toString());
            sendData.put("time", edit_Time.getText().toString());
            sendData.put("capacity", capacity.getText().toString());

            if (super.isNetworkAvailable(this)) {
                RegisterEvent event = new RegisterEvent(this);
                result = event.execute(sendData).get();

                if (Integer.parseInt(result) == 1) {
                    Toast.makeText(this, "You have successfully created an event", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateEvent.this, HomePage.class);
                    startActivity(intent);
                } else
                    Toast.makeText(this, "There was an error processing your request", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();
            }
        }
    }


    //Check to see activity name includes only letters
    public boolean checkInput(String input, String inputtime, String date, String location)
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy",Locale.US);
        String currentDate = df.format(c.getTime());
        boolean flag = false;
        boolean timeflag = true;
        String[] curtemp = currentDate.split("/");
        String[] temp = date.split("/");
        Integer[] curtempi = {0,0,0};
        Integer[] plantempi = {0,0,0};
        Date time = new Date();
        int curhour = time.getHours();
        int curmin = time.getMinutes();
        String[] temptime = inputtime.split(":");
        int inputhour = Integer.parseInt(temptime[0]);
        int inputmin = Integer.parseInt(temptime[1]);

        for(int i=2; i>=0; i--)
        {
            curtempi[i] = Integer.parseInt(curtemp[i]);
            plantempi[i] = Integer.parseInt(temp[i]);



        }

        if(plantempi[2] > curtempi[2])
        {
            flag = true;
        }
        else if(plantempi[2] == curtempi[2])
        {
            if(plantempi[0] > curtempi[0])
            {
                flag = true;
            }
            else if(plantempi[0] == curtempi[0])
            {
                if(plantempi[1] > curtempi[1])
                    flag = true;
                else if(plantempi[1] == curtempi[1])
                {
                    flag = true;
                    if(inputhour > curhour)
                        timeflag = true;
                    else if(inputhour == curhour)
                    {
                        if(inputmin > curmin)
                            timeflag = true;
                        else
                            timeflag = false;
                    }
                    else
                        timeflag = false;

                }
            }
        }

        if(flag == false)
        {
            Toast.makeText(this,"Invalid date", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(timeflag == false)
        {
            Toast.makeText(this,"Invalid time",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(input.matches("[a-zA-z ]+") && location.matches("[a-zA-Z0-9 ]+"))
            return true;
        else {
            Toast.makeText(this,"Event name allows letters only and Location allows number and letters",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    //To put the spinner items in the list and item selected listen inside.
    private void setActivitySpinnerItems()
    {
        activityList = new ArrayList<>();
        activitySpinner = (Spinner) findViewById(R.id.activity_spinner);
        activityList.add("Basketball");
        activityList.add("Football");
        activityList.add("Chess");
        activityList.add("Pokemon Go");
        activityList.add("Tennis");
        Collections.sort(activityList, new Comparator<String>()
        {
            @Override
            public int compare(String text1, String text2)
            {
                return text1.compareToIgnoreCase(text2);
            }
        });
        activityList.add("OTHER");
        activityAdapter = new ArrayAdapter<>(this,R.layout.activity_spinner_items,activityList);
        activityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activitySpinner.setAdapter(activityAdapter);

        activitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                activityName = activitySpinner.getItemAtPosition(position).toString();


                if(activityName.equals("OTHER"))
                {
                    add_activity.setEnabled(true);
                }
                else
                {
                    add_activity.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

   public void popUpCalendar(View v)
   {
       myCalendar = Calendar.getInstance();
       edit_Date = (EditText) findViewById(R.id.event_date);

       final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

           @Override
           public void onDateSet(DatePicker view, int year, int monthOfYear,
                                 int dayOfMonth) {
               // TODO Auto-generated method stub
               myCalendar.set(Calendar.YEAR, year);
               myCalendar.set(Calendar.MONTH, monthOfYear);
               myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
               updateLabel();
           }

       };

       edit_Date.setOnClickListener(new View.OnClickListener() {

           @Override
           public void onClick(View v) {

               // TODO Auto-generated method stub
               new DatePickerDialog(CreateEvent.this, date, myCalendar
                       .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                       myCalendar.get(Calendar.DAY_OF_MONTH)).show();
               //updateLabel();
           }
       });

   }

    private void updateLabel()
    {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edit_Date.setText(sdf.format(myCalendar.getTime()));
    }

    public void popUpTime(View view)
    {
        edit_Time = (EditText) findViewById(R.id.event_time);
        edit_Time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEvent.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String time = String.format(Locale.getDefault(),"%02d:%02d",selectedHour,selectedMinute);
                            edit_Time.setText(time);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
    }


    class RegisterEvent extends AsyncTask<JSONObject, Void, String>
    {
        private final Context mContext;
        private String response;

        RegisterEvent(Context context){
            this.mContext = context;
        }
        protected String doInBackground(JSONObject... params)
        {
            try{
                JSONObject input = params[0];
                URL url = new URL ("https://buddyupteam.herokuapp.com/bu_create_evt");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(input.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                StringBuffer sb = new StringBuffer();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String read;

                while((read= br.readLine())!=null)
                {
                    sb.append(read);

                }
                br.close();
                response = sb.toString();
            }
            catch(IOException e) {
                Log.d("BuddyUp", "IO EXCEPTION " + e.toString());

            }
            return response;
        }
    }

}
