package deng.raymond.buddyup;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;


/**
 * Activity for Register.xml page
 */
public class RegisterActivity extends BaseActivity {

    private Intent register_login;
    private String result;
    private JSONObject data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.register);
        super.onCreate(savedInstanceState);

        data = new JSONObject();
    }
    //Thread that Creates User
    class CreateUser extends AsyncTask<String, Void, String> {
        private final Context mContext;

        CreateUser(Context context){
            this.mContext = context;
        }

        protected String doInBackground(String... params) {

            try{
                URL url = new URL ("https://buddyupteam.herokuapp.com/bu_register");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(data.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                StringBuffer sb = new StringBuffer();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String read;

                while((read= br.readLine())!=null)
                {
                    sb.append(read);

                }
                br.close();
                result = sb.toString();

            }
            catch(IOException e) {
                Log.d("BuddyUp", "Error: " + e.toString());

            }

            return result;
        }
    }
    //User sanitation and Submit Register Request
    public void userRegister(View v) throws JSONException,InterruptedException,ExecutionException
    {
        EditText editbox;
        String key;
        String value;
        Boolean validEntries = true;

        for(int i=0;i<5;i++) {
            switch(i) {
                case 0: editbox = (EditText)findViewById(R.id.fname_input); key="first"; break;
                case 1: editbox = (EditText)findViewById(R.id.lname_input); key="last"; break;
                case 2: editbox = (EditText)findViewById(R.id.phone_input); key="phone"; break;
                case 3: editbox = (EditText)findViewById(R.id.email_input); key="email"; break;
                case 4: editbox = (EditText)findViewById(R.id.password_input); key="password"; break;
                default: editbox=null; key=null; break;
            }

            value = editbox.getText().toString();

            if(checkInput(value,i))
            {
                data.put(key,value);
            }
            else
            {
                validEntries = false;
            }

        }

        if(validEntries) {
            if(super.isNetworkAvailable(this))
            {
                CreateUser register = new CreateUser(this);
                String result = register.execute().get();
                Toast.makeText(this, "You've successfully created an account", Toast.LENGTH_SHORT).show();
                register_login = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(register_login);
            }
            else
                Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();

        }
    }
    //Method to check if the input is clean before sending
    private boolean checkInput(String input, int num)
    {
        String message;
        boolean valid;

        switch(num)
        {
            case 0: valid = input.matches("[a-zA-z]+");
                    message = "Invalid First Name: Letters ONLY!";
                    break;
            case 1: valid = input.matches("[a-zA-z]+");
                    message = "Invalid Last Name: Letters ONLY!";
                    break;
            case 2: valid = input.matches("[0-9]{10}+");
                    message = "Invalid Phone Number: 10 Numbers ONLY!";
                    break;
            case 3: valid = input.matches("[0-9a-zA-Z\\._]+@{1}[0-9a-zA-Z]+\\.{1}(com|edu|net)");
                    message = "Invalid Email: Letters and Numbers ONLY!";
                    break;
            case 4: valid = input.matches("[\\S]+");
                    message = "Invalid Password: NO SPACES!";
                    break;
            default:    valid=false;
                        message="ERROR!";
                        break;
        }

        if(!valid)
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
        // Log.v("BuddyUp", message); Debugging Message

        return valid;
    }
    //onClickListener to return to HomePage
    public void cancelRegister(View v)
    {
        Log.v("BuddyUp", "BUTTON CANCEL PRESSED");
        register_login = new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(register_login);
    }
    //onClickListener to display password
    public void displayPass(View v) throws NullPointerException
    {
        CheckBox chkShow = (CheckBox)findViewById(R.id.checkShowPassword);
        EditText passwordField = (EditText)findViewById(R.id.password_input);

        if(chkShow.isChecked())
            passwordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        else
            passwordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

}
