package deng.raymond.buddyup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyEvent extends BaseActivity{
    PopupWindow myWindow;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        setContentView(R.layout.my_event);
        super.onCreate(savedInstanceState);
        JSONObject data = new JSONObject();
        try
        {
            data.put("u_id", LoginActivity.userID);

            if(super.isNetworkAvailable(this)) {
                GetMyEvents eventList = new GetMyEvents(this);
                String response = eventList.execute(data).get();

                JSONArray jsonResult = new JSONArray(response);
                List<Event> eventData = new ArrayList<>();

                for (int i = 0; i < jsonResult.length(); i++) {
                    JSONObject tempObject = jsonResult.getJSONObject(i);

                    Event temp_event = new Event();
                    temp_event.id = tempObject.getInt("e_id");
                    temp_event.activity = tempObject.getString("activity_name");
                    temp_event.location = tempObject.getString("location");
                    temp_event.time = tempObject.getString("e_time");
                    temp_event.date = tempObject.getString("e_date");
                    temp_event.current = tempObject.getInt("people_joined");
                    temp_event.max = tempObject.getInt("max_people");

                    eventData.add(temp_event);

                    addTableRow(temp_event);
                }
            }
            else
                Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();
        }
        catch (JSONException e)
        {
            Log.v("BuddyUp", "MYEVENT THREW JSON EXCEPTION");
        }
        catch(InterruptedException e)
        {
            Log.v("BuddyUp","MYEVENT RECEIVED AN INTERRUPTED EXCEPTION");
        }
        catch(ParseException e)
        {
            Log.v("BuddyUp","MYEVENT RECEIVED A PARSE EXCEPTION");
        }
        catch(ExecutionException e)
        {
            Log.v("BuddyUp","MYEVENT RECEIVED AN EXECUTION EXCEPTION");
        }
    }

    //Create New Row Dynamically for Event
    @SuppressLint("SimpleDateFormat")
    private void addTableRow(Event item) throws NullPointerException, ParseException
    {
        final TableLayout my_event_table = (TableLayout) findViewById(R.id.my_evt_table);
        @SuppressLint("InflateParams") final TableRow view = (TableRow)getLayoutInflater().inflate(R.layout.my_event_item, null);

        TextView my_activity = (TextView)view.findViewById(R.id.my_table_activity_field);
        my_activity.setText(item.activity);

        TextView my_location = (TextView)view.findViewById(R.id.my_table_location_field);
        my_location.setText(item.location);

        TextView my_time = (TextView)view.findViewById(R.id.my_table_time_field);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        Date inputTime = timeFormat.parse(item.time);
        timeFormat = new SimpleDateFormat("hh:mm aa");
        item.time = timeFormat.format(inputTime);
        my_time.setText(item.time);

        TextView my_date = (TextView)view.findViewById(R.id.my_table_date_field);

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date inputDate = dateFormat.parse(item.date);
        dateFormat = new SimpleDateFormat("mm/dd/yy");
        item.date = dateFormat.format(inputDate);
        my_date.setText(item.date);

        String curMax = item.current + "/" + item.max;
        TextView my_size = (TextView)view.findViewById(R.id.my_table_size_count);
        my_size.setText(curMax);

        Button leaveBtn = (Button)view.findViewById(R.id.table_leave_btn);
        leaveBtn.setId(item.id);

        my_event_table.addView(view);
    }
    //This Thread will Get All The Events with User's ID
    class GetMyEvents extends AsyncTask<JSONObject, Void, String>
    {
            private final Context mContext;
            private String result;

            GetMyEvents(Context context){
                this.mContext = context;
            }

            protected String doInBackground(JSONObject... params) {

                try{
                    JSONObject data = params[0];
                    URL url = new URL ("https://buddyupteam.herokuapp.com/bu_my_evts");
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    urlConnection.setRequestMethod("POST");
                    urlConnection.setConnectTimeout(2000);
                    urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.connect(); //Start Connection

                    //Sending JSON to Server
                    OutputStream sendData = urlConnection.getOutputStream();
                    OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                    sendDataWriter.write(data.toString());
                    sendDataWriter.flush();
                    sendDataWriter.close();

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    StringBuffer buffer = new StringBuffer();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String read;

                    while((read= reader.readLine())!=null)
                    {
                        buffer.append(read);

                    }
                    reader.close();
                    result = buffer.toString();

                }
                catch(IOException e)
                {
                    Log.d("BuddyUp", "Error: " + e.toString());
                }

                return result;
            }
        }
    //This Thread Will Leave The Event with UID and Event ID
    class leaveEvent extends AsyncTask<JSONObject, Void, String>
    {
        private final Context mContext;
        private String result;

        leaveEvent(Context context){
            this.mContext = context;
        }

        protected String doInBackground(JSONObject... params) {

            try{
                JSONObject newData = params[0];

                URL url = new URL ("https://buddyupteam.herokuapp.com/bu_remove_user");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setConnectTimeout(2000);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.connect(); //Start Connection

                //Sending JSON to Server
                OutputStream sendData = urlConnection.getOutputStream();
                OutputStreamWriter sendDataWriter = new OutputStreamWriter(sendData, "UTF-8");

                sendDataWriter.write(newData.toString());
                sendDataWriter.flush();
                sendDataWriter.close();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String read;

                while((read= reader.readLine())!=null)
                {
                    buffer.append(read);

                }
                reader.close();
                result = buffer.toString();

            }
            catch(IOException e)
            {
                Log.d("BuddyUp", "Error: " + e.toString());
            }

            return result;
        }
    }
    //onClickListener for users who leave Events
    public void userLeaveEvent (View v) throws JSONException,InterruptedException,ExecutionException, NullPointerException
    {
        //Package JSON to send
        JSONObject inputData = new JSONObject();
        inputData.put("evt_id",v.getId());
        inputData.put("u_id", LoginActivity.userID);

        if(super.isNetworkAvailable(this))
        {
            leaveEvent leaveEventAction = new leaveEvent(this);
            String remove_response = leaveEventAction.execute(inputData).get();

            //Successfully Left Event, Sending to Home Screen
            if ((Integer.parseInt(remove_response)) == 1) {
                Intent intent = new Intent(MyEvent.this, HomePage.class);
                Toast.makeText(this, "You have been removed from the event", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
            //Throw an error message about not being able to leave
            else
                Toast.makeText(this, "You have been removed from the event", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "You are not connected to the internet",Toast.LENGTH_SHORT).show();
    }

    public void displayPopUp (View v) throws NullPointerException
    {
        ScrollView parentLayout = (ScrollView)findViewById(R.id.my_event_layout);
        parentLayout.setAlpha(0.25f);

        LayoutInflater inflater = (LayoutInflater) MyEvent.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.pop_up_window,
                (ViewGroup) findViewById(R.id.pop_up_layout));
        myWindow = new PopupWindow(layout, 1000, 1000, true);
        myWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        TableRow t = (TableRow) v;

        TextView temp;
        String tempString;
        //Build The Popup Window
        TextView popActivityName = (TextView) layout.findViewById(R.id.popActivityText);
        temp = (TextView) t.getChildAt(0);
        tempString = temp.getText().toString();
        popActivityName.setText(tempString);

        TextView popLocation = (TextView) layout.findViewById(R.id.popLocationText);
        temp = (TextView) t.getChildAt(1);
        tempString = temp.getText().toString();
        popLocation.setText(tempString);

        TextView popTime = (TextView) layout.findViewById(R.id.popTimeText);
        temp = (TextView) t.getChildAt(2);
        tempString = temp.getText().toString();
        popTime.setText(tempString);

        TextView popDate = (TextView) layout.findViewById(R.id.popDateText);
        temp = (TextView) t.getChildAt(3);
        tempString = temp.getText().toString();
        popDate.setText(tempString);

        TextView popSize = (TextView) layout.findViewById(R.id.popCountText);
        temp = (TextView) t.getChildAt(4);
        tempString = temp.getText().toString();
        popSize.setText(tempString);

    }

    public void closePopUp (View v) {
        ScrollView parentLayout = (ScrollView)findViewById(R.id.my_event_layout);
        parentLayout.setAlpha(1.0f);
        myWindow.dismiss();
    }

}